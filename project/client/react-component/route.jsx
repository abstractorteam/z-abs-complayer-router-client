
'use strict';

import {RouterContext} from './router-context';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import UrlPattern from 'url-pattern';
import React from 'react';


export default class Route extends ReactComponentBase {
  constructor(props) {
    super(props);
    this.router = {
      name: 'RouterContext',
      previousUriPart: '',
      routeUriPart: '',
      restUriPart: '',
      location: {
        hash: '',
        host: '',
        hostname: '',
        href: '',
        origin: '',
        pathname: '',
        port: '',
        protocol: '',
        search: ''
      },
      self: null
    }
    this.state.self = this;
    this.refCatchClick = React.createRef();
    this.onClick = this._onClick.bind(this);
    this.childRoute = false;
  }
  
  didMount() {
    if(this.props.catchClick && this.refCatchClick.current) {
      this.refCatchClick.current.addEventListener('click', this.onClick, false);
    }
  }
    
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state, nextState)
      || !this.shallowCompare(this.props, nextProps);
  }
  
  willUnmount() {
    if(this.props.catchClick && this.refCatchClick.current) {
      this.refCatchClick.current.removeEventListener('click', this.onClick, false);
    }
  }
  
  onSwitch(routeUriPart) {
    this.childRoute = true;
  }
  
  renderRouterContext(childProps) {
    return (
      <RouterContext.Provider value={this.router}>
        {React.createElement(this.props.handler, childProps, this.children)}
      </RouterContext.Provider>
    );
  }
  
  renderHandler(childProps) {
    if(this.props.handler) {
      if(this.props.catchClick) {
        return (
          <div ref={this.refCatchClick}>
            {this.renderRouterContext(childProps)}
          </div>
        );
      }
      else {
        return (
          <>
            {this.renderRouterContext(childProps)}
          </>
        );
      }
    }
    else {
      return null;
    }
  }
  
  render() {
    //console.log('------ Route.render ', this.props.handler.name, '------', this.props);
    let props = null;
    if(!this.props.notFound) {
      this.childRoute = false;
      const parentRestUriPart = this.context.restUriPart;
      if(!Array.isArray(this.props.switch)) {
        if(this.props.switch) {
          const urlPattern = new UrlPattern(this.props.switch);
          props = urlPattern.match(parentRestUriPart);
        }
        else if(!parentRestUriPart) {
          props = {_: ''};
        }
        if(props) {
          //console.log('YES - switch: \'' + this.props.switch + '\', parentRestUriPart \'' + parentRestUriPart + '\'', props);
        }
        else {
          //console.log('NO  - switch: \'' + this.props.switch + '\', parentRestUriPart \'' + parentRestUriPart + '\'', props);
        }
      }
      else {
        const conditions = this.props.switch;
        for(let i = 0; i < conditions.length; ++i) {
          const currentSwitch = conditions[i];
          if(currentSwitch) {
            const urlPattern = new UrlPattern(currentSwitch);
            props = urlPattern.match(parentRestUriPart);
          }
          else if(!parentRestUriPart) {
            props = {_: ''};
          }
          if(props) {
            //console.log('YES - switch: \'' + conditions[i] + '\', parentRestUriPart \'' + parentRestUriPart + '\'', props);
            break;
          }
          else {
            //console.log('NO  - switch: \'' + conditions[i] + '\', parentRestUriPart \'' + parentRestUriPart + '\'', props);
          }
        }
      }
    
      if(props) {
        const context = this.context;
        const parentPreviousUriPart = context.previousUriPart;
        const parentRouteUriPart = context.routeUriPart;
        const previousUriPart = parentPreviousUriPart + parentRouteUriPart;
        const restUriPart = props._ ?  props._ : '';
        const routeUriPart = parentRestUriPart.substring(0, parentRestUriPart.length - restUriPart.length);
        if(this.router.routeUriPart !== routeUriPart || this.router.restUriPart !== restUriPart || this.router.location !== context.location) {
          this.router = {
            name: 'RouterContext',
            previousUriPart,
            routeUriPart,
            restUriPart,
            location: context.location,
            self: this,
            history: (href, options, state) => {
              const _options = options ? options : {
                replace: false,
                replaceStay: false,
                global: false
              };
              if(undefined === _options.replace) {
                _options.replace = false;
              }
              if(undefined === _options.replaceStay) {
                _options.replaceStay = false;
              }
              if(undefined === _options.global) {
                _options.global = false;
              }
              const uri = options && options.global ? href : `${this.router.previousUriPart}${href.startsWith('/') ? '' : (href ? '/' : '')}${href}`;
              this.myContext.history(uri, _options, state ? state : {});
            },
            attachHistory: (action) => {
              action.setHistoryObject(this.router);
            }
          };
        }
      }
    }
    
    if(props || this.props.notFound) {
      /*console.log('  context:', this.context);
      console.log('  switch:', this.props.switch);
      console.log('  location:', this.router.location);
      console.log('  previousUriPart: \'' + this.router.previousUriPart + '\'');
      console.log('  routeUriPart: \'' + this.router.routeUriPart + '\'');
      console.log('  restUriPart: \'' + this.router.restUriPart + '\'');
      console.log('  parent:', this.context.self);*/
      this.context.self.onSwitch(this.router.routeUriPart);
      const childProps = {};
      Reflect.set(childProps, 'location', this.router.location);
      Reflect.ownKeys(this.props).forEach((key) => { 
        if('handler' !== key && 'switch' !== key && 'notFound' !== key) {
          Reflect.set(childProps, key, Reflect.get(this.props, key));
        }
      });
      if(props) {
        Reflect.ownKeys(props).forEach((key) => {
          if('_' === key) {
            Reflect.set(childProps, '_uriPath', this.router.restUriPart);
          }
          else if(!key.startsWith('_')) {
            Reflect.set(childProps, key, Reflect.get(props, key));
          }
        });
      }
      return (
        <>
          {this.renderHandler(childProps)}
        </>
      );
    }
    else {
      /*console.log('  context:', this.context);
      console.log('  switch:', this.props.switch);
      console.log('  location:', this.router.location);
      console.log('  previousUriPart: \'' + this.router.previousUriPart + '\'');
      console.log('  routeUriPart: \'' + this.router.routeUriPart + '\'');
      console.log('  restUriPart: \'' + this.router.restUriPart + '\'');
      console.log('  parent:', this.context.self);*/
      return null;
    }
  }
  
  _onClick(e) {
    if(e.defaultPrevented) {
      return;
    }
    if(e.metaKey || e.ctrlKey || e.shiftKey) {
      return;
    }
    let element = e.target;
    while(element && element.nodeName !== 'A') {
      element = element.parentNode;
    }
    if(!element) {
      return;
    }
    if(element.dataset['isLink']) {
      return;
    }
    if(element.target && element.target !== '_self') {
      return;
    }
    if(element.attributes.download) {
      return;
    }
    if(this.router.location.origin !== e.target.origin) {
      return;
    }
    e.stopPropagation();
    e.preventDefault();
    this.myContext.history(element, {
      replace: this.props.replace ? this.props.replace : false,
      global: this.props.global ? this.props.global : false
    }, {});
  }
}


Route.contextType = RouterContext;
