
'use strict';

import {RouterContext} from './router-context';
import ReactComponentBase from 'z-abs-corelayer-client/react-component/react-component-base';
import React from 'react';


export default class Router extends ReactComponentBase {
  constructor(props) {
    super(props, {
      name: 'RouterContext',
      previousUriPart: '',
      routeUriPart: '',
      restUriPart: '',
      location: {
        hash: window.location.hash,
        host: window.location.host,
        hostname: window.location.hostname,
        href: window.location.href,
        origin: window.location.origin,
        pathname: window.location.pathname,
        port: window.location.port,
        protocol: window.location.protocol,
        search: window.location.search
      },
      self: null
    });
    this.state.self = this;
    this.myContext.history = (href, options, state) => {
      //console.log('HISTORY', href, options, this.state.routeUriPart);
      this._updateRouterState(href, options.global, (result) => {
        if(options.replace) {
          history.replaceState(state, 'GUNNAR', result.location.href);
        }
        else {
          history.pushState(state, 'GUNNAR', result.location.href);
        }
      });
    };
    const calculatedLocation = this._calculateRouterState(window.location);
    if(null !== calculatedLocation) {
      this.state.routeUriPart = calculatedLocation.routeUriPart;
      this.state.restUriPart = calculatedLocation.restUriPart;
      this.state.history = (href, state, options) => {
        this.myContext.history(href, state, options);
      };
    }
    this.onPopState = this._onPopState.bind(this);
    this.childRoute = false;
  }

  didMount() {
    window.addEventListener('popstate', this.onPopState, true);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.state, nextState)
      || !this.shallowCompare(this.props, nextProps);
  }
    
  willUnmount() {
    window.removeEventListener('popstate', this.onPopState, true);
  }
  
  onSwitch(routeUriPart) {
    this.childRoute = true;
  }
  
  _onPopState(e) {
    e.stopPropagation();
    e.preventDefault();
    this._updateRouterState(window.location);
  }
  
  _calculateRouterState(data, global) {
    if('string' === typeof data) {
      if(global) {
        data = `${this.state.routeUriPart}${data}`
      }
      data = new URL(data);
    }
    const routeUriPart = `${data.protocol}//${data.host}`;
    const restUriPart = data.pathname;
    const href = data.href;
    if(this.state.routeUriPart !== routeUriPart || this.state.restUriPart !== restUriPart || this.state.href !== href) {
      return {
        routeUriPart,
        restUriPart,
        location: {
          hash: data.hash,
          host: data.host,
          hostname: data.hostname,
          href: data.href,
          origin: data.origin,
          pathname: data.pathname,
          port: data.port,
          protocol: data.protocol,
          search: data.search
        }
      };
    }
    return null;
  }
  
  _updateRouterState(data, global, cbBeforeUpdate) {
    const calculatedLocation = this._calculateRouterState(data, global);
    if(null !== calculatedLocation) {
      cbBeforeUpdate && cbBeforeUpdate(calculatedLocation);
      this.updateState({
        routeUriPart: {$set: calculatedLocation.routeUriPart},
        restUriPart: {$set: calculatedLocation.restUriPart},
        location: {$set: calculatedLocation.location}
      });
    }
  }
  
  render() {
    this.childRoute = false;
    /*console.log('------ Router.render ------');
    console.log('  href: \'' + this.state.href + '\'');
    console.log('  routeUriPart: \'' + this.state.routeUriPart + '\'');
    console.log('  restUriPart: \'' + this.state.restUriPart + '\'');*/
    return (
      <RouterContext.Provider value={this.state}>
        {this.props.children}
      </RouterContext.Provider>
    );
  }
  
}
