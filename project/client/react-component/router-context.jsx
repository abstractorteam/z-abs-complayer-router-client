 
'use strict';

import React from 'react';


const RouterContext = React.createContext({name: 'RouterContext'});


module.exports = {
  RouterContext: RouterContext
};
